package br.edu.unoesc.edii.hash;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.util.Scanner;

public class Hash {
	static Scanner teclado = new Scanner(System.in);
	public static void main(String[] args) {
		int opc;
		
		do {
			System.out.println("Escolha a opção desejada: \n"
					+ "1 - Inserir Registro\n"
					+ "2 - Editar Registro\n"
					+ "3 - Pesquisar Registro\n"
					+ "4 - Remover Registro\n"
					+ "0 - Sair");
			 
			 opc = teclado.nextInt();
			 switch(opc) {
			 case 1: inserirRegistro(null);
			 break;
			 case 2: pesquisarRegistro(true);
			 break;
			 case 3: pesquisarRegistro(false);
			 break;
			 case 4: removerRegistro();
			 break;
			 case 0: teclado.close();
			 System.out.println("Saindo...");
			 
			 }
		} while(opc!=0);
		teclado.close();
	}
	
	private static void removerRegistro() {
		System.out.println("Digite o cpf desejado:");
		Long cpf = teclado.nextLong();
		long hash = montarHash(cpf.toString()); 
		writeToRandomAccessFile("arquivosHash.store",hash, "");
		
	}

	private static String pesquisarRegistro(Boolean editarRegistro) {
		System.out.println("Digite o cpf desejado:");
		Long cpf = teclado.nextLong();
		long hash = montarHash(cpf.toString()); 
		String registro = readFromRandomAccessFile("arquivosHash.store",hash);
		if(editarRegistro) {
			inserirRegistro(cpf);
		} else {
			System.out.println(registro);
		}
		
		return registro;
	}

	private static void inserirRegistro(Long cpfParam) {
		System.out.println("nome:");
		String nome = "nome:" + teclado.next() + ";";
		String cpf = "";
		if(cpfParam == null) {
			System.out.println("cpf:");
			cpf = "cpf:" + teclado.next() + ";";	
		} else {
			cpf = "cpf:" + cpfParam + ";";
		}
		System.out.println("data de nascimento:");
		String dataNascimento = "dataNascimento:" + teclado.next() + ";";
		String record = nome + cpf + dataNascimento + System.lineSeparator();
		Long numeroHash = montarHash(cpf);
		writeToRandomAccessFile("arquivosHash.store",numeroHash, record);
	}
	
	private static long montarHash(String cpf) {
		Long numeroCpf = montarCpf(cpf);
		BigDecimal hash = new BigDecimal(numeroCpf % 397);
		String concatHash = hash.toString();
		concatHash.replaceAll(".", "");
		return new Long(concatHash);
	}
	
	public static String montarSubstring(String chave, String linha) {
		int posicaoInicial = linha.indexOf(chave);
		int posicaoFinal = linha.indexOf(";", posicaoInicial);
		return linha.substring(posicaoInicial+chave.length(), posicaoFinal);
	}
	
	public static Long montarCpf(String cpf) {
		cpf = cpf.replaceAll("cpf:", "");
		cpf = cpf.replaceAll("\\.", "");
		cpf = cpf.replaceAll("\\-", "");
		cpf = cpf.replaceAll(";", "");
		return new Long(cpf);
	}
	
	 /*
     * Utility method to read from RandomAccessFile in Java
     */
    public static String readFromRandomAccessFile(String file, long position) {
        String record = null;
        try {
            RandomAccessFile fileStore = new RandomAccessFile(file, "rw");

            // moves file pointer to position specified
            fileStore.seek(position);

            // reading String from RandomAccessFile
            record = fileStore.readUTF();

            fileStore.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return record;
    }

   /*
    * Utility method for writing into RandomAccessFile in Java
    */  
    public static void writeToRandomAccessFile(String file, long position, String record) {
        try {
            RandomAccessFile fileStore = new RandomAccessFile(file, "rw");

            // moves file pointer to position specified
            fileStore.seek(position);

            // writing String to RandomAccessFile
            fileStore.writeUTF(record);

            fileStore.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
